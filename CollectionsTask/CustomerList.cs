using System;
using System.Collections;
using System.Collections.Generic;



/*
    Problem #2
-----------------------------------------------------------------------------------------

Replace the custom generic collection class with List<T> and do the same operations.

*/


public class Customer: IComparable<Customer>
    {
		protected Guid? _UniqueId;
        private string _FirstName = "";
        private string _LastName = "";
		private string _Address;
		private int _Age;

        //Paramaterized constructor for immediate instantiation
        public Customer(string first, string last, string address, int age)
        {
			_UniqueId = Guid.NewGuid();
            _FirstName = first;
            _LastName = last;
			_Address=address;
			_Age=age;
        }

        //Default constructor
        public Customer()
        {
            //nothing
        }
		
		public Guid? UniqueId
        {
            get
            {
                return _UniqueId;
            }
            set
            {
                _UniqueId = value;
            }
        }

        //Customer' First Name 
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        //Customer's Last Name
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }
    
	
	public string Address
        {
            get
            {
                return _Address;
            }
            set
            {
                _Address = value;
            }
        }
    

    public int Age
        {
            get
            {
                return _Age;
            }
            set
            {
                _Age = value;
            }
        }
    
	public int CompareTo( Customer other )
     {
         if ( this.Age > other.Age ) return 1;
         else if ( this.Age < other.Age ) return -1;
         else return 0;
     }
	


	}
	
	class CustomerList
	{
	
	 
	  
	 public static void Main(string[] args)
	 {
	    int age,choice,index;
		bool flag=false;
	  string first,last,add;	 
	  List<Customer> customer=new List<Customer>();
	  
	     
	  
	       do
			{
			 Console.WriteLine("-----------------------------------------------------------------------------");	
			 Console.WriteLine("1.Add customer");
			 Console.WriteLine("2.Remove customer");
			 Console.WriteLine("3.Search customer");
			 Console.WriteLine("4.Sort customer");
			 Console.WriteLine("5.List customer");
			 Console.WriteLine("6.Exit");
			 Console.WriteLine("-----------------------------------------------------------------------------");
			 Console.WriteLine("Enter your choice");
			 int.TryParse(Console.ReadLine(),out choice);
			 
			 
			 
			 switch(choice)
			 {
				 case 1:
				 
				 //Add Customer
				 Console.WriteLine("Enter Customer first name");
				 first=Console.ReadLine();
				 Console.WriteLine("Enter Customer last name");
				 last=Console.ReadLine();
				 Console.WriteLine("Enter Customer Address");
				 add=Console.ReadLine();
				 Console.WriteLine("Enter Customer Age");
				 int.TryParse(Console.ReadLine(),out age);
				 Customer obj=new Customer(first,last,add,age);
				
				 customer.Add(obj);
				 
				 break;
				 
				 case 2:
				 
				 Console.WriteLine("Enter Customer index to be removed");
				 int.TryParse(Console.ReadLine(),out index);
				 
				 customer.RemoveAt(index);
				 
				 break;
				 
				 case 3:
				 
				 Console.WriteLine("Enter Customer first name to be searched");
				 first=Console.ReadLine();
				 
					foreach(Customer c in customer)
					{
						if(c.FirstName==first)
						{
							flag=true;
					    Console.WriteLine("Element found");
						break;
						}
					}
					if(flag==false)
					Console.WriteLine("Element not found");
					break;
				 
				 case 4:
				 
				 customer.Sort();
				 Console.WriteLine("List sorted.");
				 
				 
				 break;
				 
				 case 5:
				 
				 foreach (var c in customer)
                  {
					Console.WriteLine("-----------------------------------------------------------------------------");
                    Console.WriteLine("Name:"+ c.FirstName + " " + c.LastName);
					Console.WriteLine("Address:"+" " + c.Address);
					Console.WriteLine("Age:"+" " + c.Age);
					Console.WriteLine("Customer Id:"+" " + c.UniqueId);
					Console.WriteLine("-----------------------------------------------------------------------------");
                  }
				 
				 break;
				 
				 
				 
			 }
			 
			 
			 
		 
			 
	
			 
			}while(choice!=6);
	  
	     
      
	 }
	
	
	
	
	}