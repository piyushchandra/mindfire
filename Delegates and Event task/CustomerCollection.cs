
/*
  Problem #1
----------------------------------------------------------------------------------------

Create a Customer class having following properties.
1.CustomerId
2.FirstName
3.LastName
4.Age
5.Address

Create a custom generic collection class , which will be used for below operations.

1.Adding a customer to the custom generic collection.
2.Removing a customer from the custom generic collection.
3.Searching a customer in the custom generic collection.
4.Listing all the customers in the custom generic collection.
5.Sorting the customer collection based on custom order.

*/

using System;
using System.Collections;
using System.Collections.Generic;


public class Customer: EventArgs, IComparable
    {
		private int _CustomerId;
        private string _FirstName = "";
        private string _LastName = "";
		private string _Address;
		private int _Age;
		
        //static variable ComparisonType;
		public static int ComparisonType;
		
        //Paramaterized constructor for immediate instantiation
        public Customer(int id,string first, string last, string address, int age)
        {
			_CustomerId = id;
            _FirstName = first;
            _LastName = last;
			_Address=address;
			_Age=age;
        }
	
		
        //Default constructor
        public Customer()
        {
            //nothing
        }
		
		//Customer' Id 
		public int CustomerId
        {
            get
            {
                return _CustomerId;
            }
            set
            {
                _CustomerId = value;
            }
        }

        //Customer' First Name 
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        //Customer's Last Name
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }
    
	//Customer' Address 
	public string Address
        {
            get
            {
                return _Address;
            }
            set
            {
                _Address = value;
            }
        }
    
    //Customer' Age 
    public int Age
        {
            get
            {
                return _Age;
            }
            set
            {
                _Age = value;
            }
        }
		
		
		
		public int CompareTo(object obj)
		{
			Customer other = obj as Customer;
			switch (ComparisonType)
            {
				default:
				
                case 1:
				
                    return this.FirstName.CompareTo(other.FirstName);
					
                case 2:
				
                    return this.LastName.CompareTo(other.LastName);
					
				case 3:
				
				    if(this.Age > other.Age ) return 1;
					else if( this.Age < other.Age ) return -1;
					else return 0;
				    
                case 4:			
				
				    return this.Address.CompareTo(other.Address);
			
				case 5:
				
				    if(this.CustomerId > other.CustomerId ) return 1;
					else if( this.CustomerId < other.CustomerId ) return -1;
					else return 0;
			
            }
			
		}
  
	}
		
		//Our Custom Collection 
	public class CustomerCollection<T> where T : Customer
    {
        //inner ArrayList object
        protected ArrayList _innerArray;
		
		//delegate 
		public delegate void CustomerChangeHandler(object add, Customer Information);
		// an instance of the delegate
        public event CustomerChangeHandler CustomerAdded;
		public event CustomerChangeHandler CustomerRemoved;
		
		
        
        // Default constructor
        public CustomerCollection()
        {
            _innerArray = new ArrayList();
        }

        // Default accessor for the collection 
        public T this[int index]
        {
            get
            {
                return (T)_innerArray[index];
            }
            set
            {
                _innerArray[index] = value;
            }
        }

        // Number of elements in the collection
        public virtual int Count
        {
            get
            {
                return _innerArray.Count;
            }
        }
		
		
		
		public void Sort()
		{
		  _innerArray.Sort();
		}

        


        // Add a Customer object to the collection
        public void Add(T CustomerObject)
        {
            _innerArray.Add(CustomerObject);
			
		//	EventArgs info=new EventArgs();
			
		         // if anyone has subscribed, notify them
                    if (CustomerAdded != null)
                    {
                        CustomerAdded(this,CustomerObject);
                    }
			
        }

      
	    
        public bool RemoveByFirstName(string first)
        {
            bool result = false;
            T obj=null;
            //loop through the inner array's indices
            for (int i = 0; i < _innerArray.Count; i++)
            {
                //store current index being checked
                obj = (T)_innerArray[i];

              //compare the Customer FirstName property
                if (obj.FirstName == first)
                {
                    //remove item from inner ArrayList at index i
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }
			
			
							 if (CustomerRemoved != null)
                             {
                                CustomerRemoved(this,obj);
                              }
			
			
			
			
			

            return result;
        }
		
		public bool RemoveByLastName(string last)
        {
            bool result = false;
            T obj=null;
            //loop through the inner array's indices
            for (int i = 0; i < _innerArray.Count; i++)
            {
                //store current index being checked
                 obj = (T)_innerArray[i];

                //compare the Customer LastName property
                if (obj.LastName == last)
                {
                    //remove item from inner ArrayList at index i
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }
			
			  if (CustomerRemoved != null)
                             {
                                CustomerRemoved(this,obj);
                              }

            return result;
        }
		
		public bool RemoveByAddress(string address)
        {
            bool result = false;
             T obj=null;
            //loop through the inner array's indices
            for (int i = 0; i < _innerArray.Count; i++)
            {
                //store current index being checked
                 obj = (T)_innerArray[i];

                //compare the CustomerObjectBase UniqueId property
                if (obj.Address == address)
                {
                    //remove item from inner ArrayList at index i
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }
			
			                 if (CustomerRemoved != null)
                              {
                                CustomerRemoved(this,obj);
                              }

            return result;
        }
		
		public bool RemoveByAge(int age)
        {
            bool result = false;
            T obj=null;
            //loop through the inner array's indices
            for (int i = 0; i < _innerArray.Count; i++)
            {
                //store current index being checked
                 obj = (T)_innerArray[i];

                //compare the Customer Age property
                if (obj.Age == age)
                {
                    //remove item from inner ArrayList at index i
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }
			
			                 if (CustomerRemoved != null)
                              {
                                CustomerRemoved(this,obj);
                              }

            return result;
        }
		
		public bool RemoveByCustomerId(int id)
        {
            bool result = false;
            T obj=null;
            //loop through the inner array's indices
            for (int i = 0; i < _innerArray.Count; i++)
            {
                //store current index being checked
                 obj = (T)_innerArray[i];

                //compare the Customer CustomerId property
                if (obj.CustomerId == id)
                {
                    //remove item from inner ArrayList at index i
                    _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }
               if (CustomerRemoved != null)
                              {
                                CustomerRemoved(this,obj);
                              }
			   
			   
            return result;
        }
		
		
		public int SearchByFirstName(string first)
		{
			 //loop through the inner ArrayList
            for(int i=0;i<_innerArray.Count;i++)
            {
				//store current index being checked
                T obj = (T)_innerArray[i];
				
                //compare the Customer FirstName property
                if (obj.FirstName == first)
                {
                    //if it matches return true
                    return i;
                }
            }
            //no match
            return -1;
			
		}
		
		
		
		public int SearchByLastName(string last)
		{
			 //loop through the inner ArrayList
            for(int i=0;i<_innerArray.Count;i++)
            {
				//store current index being checked
                T obj = (T)_innerArray[i];
				
                //compare the Customer LastName property
                if (obj.LastName == last)
                {
                    //if it matches return true
                    return i;
                }
            }
            //no match
            return -1;
			
		}
		
		public int SearchByAddress(string address)
		{
			 //loop through the inner ArrayList
            for(int i=0;i<_innerArray.Count;i++)
            {
				//store current index being checked
                T obj = (T)_innerArray[i];
				
                //compare the Customer Address property
                if (obj.Address == address)
                {
                    //if it matches return true
                    return i;
                }
            }
            //no match
            return -1;
			
		}
		
		public int SearchByAge(int age)
		{
			 //loop through the inner ArrayList
            for(int i=0;i<_innerArray.Count;i++)
            {
				//store current index being checked
                T obj = (T)_innerArray[i];
				
                //compare the Customer Age property
                if (obj.Age == age)
                {
                    //if it matches return true
                    return i;
                }
            }
            //no match
            return -1;
			
		}
		
		public int SearchByCustomerId(int id)
		{
			 //loop through the inner ArrayList
            for(int i=0;i<_innerArray.Count;i++)
            {
				//store current index being checked
                T obj = (T)_innerArray[i];
				
                //compare the Customer CustomerId property
                if (obj.CustomerId == id)
                {
                    //if it matches return true
                    return i;
                }
            }
            //no match
            return -1;
			
		}
		

      
        // Returns custom generic enumerator for this CustomerCollection
        public virtual IEnumerator<T> GetEnumerator()
        {
            //return a custom enumerator object instantiated
            //to use this CustomerCollection 
            return new CustomerEnumerator<T>(this);
        }
		
    }
	
	    public class CustomerEnumerator<T> : IEnumerator<T> where T : Customer
       {
        protected CustomerCollection<T> _collection; //enumerated collection
        protected int index; //current index
        protected T _current; //current enumerated object in the collection

        // Default constructor
        public CustomerEnumerator()
        {
            //nothing
        }

        // Paramaterized constructor which takes
        // the collection which this enumerator will enumerate
        public CustomerEnumerator(CustomerCollection<T> collection)
        {
            _collection = collection;
            index = -1;
            _current = default(T);
        }

        // Current Enumerated object in the inner collection
        public virtual T Current
        {
            get
            {
                return _current;
            }
        }

        // Explicit non-generic interface implementation for IEnumerator
        // (extended and required by IEnumerator<T>)
        object IEnumerator.Current
        {
            get
            {
                return _current;
            }
        }

        // Dispose method
        public virtual void Dispose()
        {
            _collection = null;
            _current = default(T);
            index = -1;
        }

        // Move to next element in the inner collection
        public virtual bool MoveNext()
        {
            //make sure we are within the bounds of the collection
            if (++index >= _collection.Count)
            {
                //if not return false
                return false;
            }
            else
            {
                //if we are, then set the current element
                //to the next object in the collection
                _current = _collection[index];
            }
            //return true
            return true;
        }

        // Reset the enumerator
        public virtual void Reset()
        {
            _current = default(T); //reset current object
            index = -1;
        }
    }
	
	public class DisplayEvent
    {
        // subscribe to CustomerChangeHandler event
        public void Subscribe(CustomerCollection<Customer> theCustomer)
        {
            theCustomer.CustomerAdded += CustomerHasAdded;
			theCustomer.CustomerRemoved += CustomerHasRemoved;
             //    new theCustomer.CustomerChangeHandler(CustomerHasChanged);
        }

        // the method that implements the
        // delegated functionality
        public void CustomerHasAdded(object theCustomer, Customer i)
        {
            Console.WriteLine("CustomerAdd Event Generated");
        }
		public void CustomerHasRemoved(object theCustomer, Customer i)
        {
            Console.WriteLine("CustomerRemove Event Generated");
        }
		
    }
	
	
	
	
	
	



internal class Program
    {
        private static void Main(string[] args)
        {
            int choice=0,age=0,type=0,id=0;
			string first="",last="",address="";
			
			 CustomerCollection<Customer> ObjList=new CustomerCollection<Customer>(); 
			 DisplayEvent d=new DisplayEvent();
			  d.Subscribe(ObjList);
			do
			{
			 Console.WriteLine("-----------------------------------------------------------------------------");	
			 Console.WriteLine("1.Add customer");
			 Console.WriteLine("2.Remove customer");
			 Console.WriteLine("3.Search customer");
			 Console.WriteLine("4.Sort customer");
			 Console.WriteLine("5.List customer");
			 Console.WriteLine("6.Exit");
			 Console.WriteLine("-----------------------------------------------------------------------------");
			 Console.WriteLine("Enter your choice");
			 int.TryParse(Console.ReadLine(),out choice);
			 
			 
			 
			 switch(choice)
			 {
				 case 1:
				 
				 Console.WriteLine("Enter Customer first name");
				 first=Console.ReadLine();
				 Console.WriteLine("Enter Customer last name");
				 last=Console.ReadLine();
				 Console.WriteLine("Enter Customer Address");
				 address=Console.ReadLine();
				 Console.WriteLine("Enter Customer Age");
				 int.TryParse(Console.ReadLine(),out age);
				 Console.WriteLine("Enter Customer Id");
				 int.TryParse(Console.ReadLine(),out id);
				 Customer obj=new Customer(id,first,last,address,age);
				
				 ObjList.Add(obj);
				 
				 break;
				 
				 case 2:
				     
					 
				    Console.WriteLine("1.Remove by First Name");
					Console.WriteLine("2.Remove by Last Name");
					Console.WriteLine("3.Remove by Age");
					Console.WriteLine("4.Remove by Address");
					Console.WriteLine("5.Remove by Customer Id");
		
		             int.TryParse(Console.ReadLine(),out type);
					 if(type==1)
					{
					 Console.WriteLine("Enter Customer first name to be removed");
					 first=Console.ReadLine();
					 ObjList.RemoveByFirstName(first);
					}
				    if(type==2)
					{
					 Console.WriteLine("Enter Customer last name to be removed");
					 last=Console.ReadLine();
					 ObjList.RemoveByLastName(first);
					}
					if(type==3)
					{
					 Console.WriteLine("Enter Customer Age to be removed");
					 int.TryParse(Console.ReadLine(),out age);
					 ObjList.RemoveByAge(age);
					}
				    if(type==4)
					{
					 Console.WriteLine("Enter Customer address to be removed");
					 address=Console.ReadLine();
					 ObjList.RemoveByAddress(address);
					}
					if(type==5)
					{
					 Console.WriteLine("Enter Customer id to be removed");
					 int.TryParse(Console.ReadLine(),out id);
					 ObjList.RemoveByCustomerId(id);
					}
					
                     Console.WriteLine("Customer Removed");
					 
					 
				 
				 break;
				 
				 case 3:
				 
				 
				    Console.WriteLine("1.Search by First Name");
					Console.WriteLine("2.Search by Last Name");
					Console.WriteLine("3.Search by Age");
					Console.WriteLine("4.Search by Address");
					Console.WriteLine("5.Search by Customer Id");
					
					int.TryParse(Console.ReadLine(),out type);
					 if(type==1)
					{
					 Console.WriteLine("Enter Customer first name to be Searched");
					 first=Console.ReadLine();
					 var i=ObjList.SearchByFirstName(first);
					 if(i==-1) 
					  Console.WriteLine("Element not found");
                      else				  
						Console.WriteLine("Element found at index"+" "+i); 
					}
				    if(type==2)
					{
					 Console.WriteLine("Enter Customer last name to be Searched");
					 last=Console.ReadLine();
					 var i=ObjList.SearchByLastName(last);
					 if(i==-1) 
					  Console.WriteLine("Element not found");
                      else				  
						Console.WriteLine("Element found at index"+" "+i);
					}
					if(type==3)
					{
					 Console.WriteLine("Enter Customer Age to be Searched");
					 int.TryParse(Console.ReadLine(),out age);
					 var i=ObjList.SearchByAge(age);
					 if(i==-1) 
					  Console.WriteLine("Element not found");
                      else				  
						Console.WriteLine("Element found at index"+" "+i);
					}
				    if(type==4)
					{
					 Console.WriteLine("Enter Customer address to be Searched");
					 address=Console.ReadLine();
					 var i=ObjList.SearchByAddress(address);
					 if(i==-1) 
					  Console.WriteLine("Element not found");
                      else				  
						Console.WriteLine("Element found at index"+" "+i);
					}
				 
				    if(type==5)
					{
					 Console.WriteLine("Enter Customer id to be Searched");
					 address=Console.ReadLine();
					 var i=ObjList.SearchByCustomerId(id);
					 if(i==-1) 
					  Console.WriteLine("Element not found");
                      else				  
						Console.WriteLine("Element found at index"+" "+i);
					}
				 
				 break;
				 
				 case 4:
				 
				    
					Console.WriteLine("1.Sort by First Name");
					Console.WriteLine("2.Sort by Last Name");
					Console.WriteLine("3.Sort by Age");
					Console.WriteLine("4.Sort by Address");
					Console.WriteLine("5.Sort by Customer Id");
					
					int.TryParse(Console.ReadLine(),out type);
				    if(type==1)
					{
						Customer.ComparisonType=1;
					}
				    if(type==2)
					{
						Customer.ComparisonType=2;
					}
					if(type==3)
					{
						Customer.ComparisonType=3;
					}
				    if(type==4)
					{
						Customer.ComparisonType=4;
					}
					if(type==5)
					{
						Customer.ComparisonType=5;
					}
					
				    Console.WriteLine("Customer List Sorted ");
				   
				   ObjList.Sort();
				 
				 
				 
				 break;
				 
				 case 5:
				 
				 foreach (var customer in ObjList)
                  {
					Console.WriteLine("-----------------------------------------------------------------------------");
                    Console.WriteLine("Name:"+ customer.FirstName + " " + customer.LastName);
					Console.WriteLine("Address:"+" " + customer.Address);
					Console.WriteLine("Age:"+" " + customer.Age);
					Console.WriteLine("Customer Id:"+" " + customer.CustomerId);
					Console.WriteLine("-----------------------------------------------------------------------------");
                  }
				 
				 break;			 
				 
			 }
			 
					 
			}while(choice!=6);
			
        
            
            Console.Read();
        }
	}