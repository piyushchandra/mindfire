  $(document).ready(function()
  {
	  
	  
    var nam=false,phon=false,emai=false;
	var score_10th=false,score_12th=false,gcollege=false,gyear=false,gstream=false,resume=false;

      
	  $("#name").focus();
	  
	  
      $("#name").focusout(function()
	  {
        validate_name();
    
      });
	  
	  
	  $("#email").focusout(function()
	  {
	  
	    validate_email();
	  });
	  
	  
	  
	  $("#phone").focusout(function()
	  {
	  
	    validate_phone();
	  });
	  
	  $("#day").focusout(function(){
		  	
		  validate_day();
		
		});
		
	  function validate_day()
	  {
		  var i=$("#day").val();
		  
	      if(i=="Day")
	      {
			
			$("#_day").html("select day");
			$("#_day").show();
			}
		 else{
			
			$("#_day").hide();
		}
		  
	  }
	  
	  $("#month").focusout(function(){
		  	
		  validate_month();
		
		});
		
	  function validate_month()
	  {
		  var i=$("#month").val();
		  
	      if(i=="Month")
	      {
			
			$("#_month").html("select Month");
			$("#_month").show();
			}
		 else{
			
			$("#_month").hide();
		}
		  
	  }
	  
	  $("#year").focusout(function(){
		  	
		  validate_year();
		
		});
		
	  function validate_year()
	  {
		  var i=$("#year").val();
		  
	      if(i=="Year")
	      {
			
			$("#_year").html("select Year");
			$("#_year").show();
			}
		 else{
			
			$("#_year").hide();
		}
		  
	  }
	  
	  
	  
	  
	  
	  $("#fresher10th").focusout(function()
	  {
		 validate_fresher10th(); 
		  
	    
	  });
	  
	  function validate_fresher10th()
	  { 
	     var score=$("#fresher10th").val();
	     if(score.length<1)
	     {
					
			$("#_fresher10th").html("Please enter 10th score");
			$("#_fresher10th").show();
			}
			
		  else if(score<30 || score>100)
		  {
			$("#_fresher10th").html("Please enter 10th percentage between 30 and 100");
			$("#_fresher10th").show();
		  }
		  
		else
		{
			score_10th=true;
			$("#_fresher10th").hide();
			}
		  
	  }
	  
	  
	  
	  
	  $("#fresher12th").focusout(function()
	  {
		  validate_fresher12th();
		  
		  
	    
	  });
	  
	  function validate_fresher12th(){
		  
		  var score=$("#fresher12th").val();
	  if(score.length<1)
	    {
			
			$("#_fresher12th").html("Please enter 12th score");
			$("#_fresher12th").show();
			}
		else if(score<30 || score>100){
			$("#_fresher12th").html("Please enter 12th percentage between 30 and 100");
			$("#_fresher12th").show();
		}
		else{
			score_12th=true;
			$("#_fresher12th").hide();
			}
		  
		  
	  }
	 
	  
	  $("#lt1year10th").focusout(function()
	  {
		  validate_lt1year10th();
		  
	  });
	  
		function validate_lt1year10th(){
		  var score=$("#lt1year10th").val();
	  if(score.length<1)
	    {
			score_10th=false;
			$("#_lt1year10th").html("Please enter 10th score");
			$("#_lt1year10th").show();
			}
		else if(score<30 || score>100){
			score_10th=false;
			$("#_lt1year10th").html("Please enter 10th percentage between 30 and 100");
			$("#_lt1year10th").show();
		}	
	    else{
			score_10th=true;
			$("#_lt1year10th").hide();
			}
		  
	  }

	 
	  $("#lt1year12th").focusout(function()
	  {
		  validate_lt1year12th();
		  
	  });
	  
	  function validate_lt1year12th()
	  {
		  var score=$("#lt1year12th").val();
	  if(score.length<1)
	    {
		//	score_12th=false;
			$("#_lt1year12th").html("Please enter 12th score");
			$("#_lt1year12th").show();
			}
		else if(score<30 || score>100){
		//	score_12th=false;
			$("#_lt1year12th").html("Please enter 12th percentage between 30 and 100");
			$("#_lt1year12th").show();
		}	
	    else{
			score_12th=true;
			$("#_lt1year12th").hide();
			}
		  
	  }
	  
	  
	  
	  $("#fresher_graduation_score").focusout(function()
	  {
		  
		  validate_fresher_graduation_score();
	
		});
		
		function validate_fresher_graduation_score()
		{
		  
		  var gscore=$("#fresher_graduation_score").val();
	  if(gscore.length<1)
	    {
			
			$("#_fresher_graduation_score").html("Please enter graduation score");
			$("#_fresher_graduation_score").show();
			}
		else if(gscore<30 || gscore>100){
			$("#_fresher_graduation_score").html("Please enter graduation percentage between 30 and 100");
			$("#_fresher_graduation_score").show();
		}	
	    else{
			gscore=true;
			$("#_fresher_graduation_score").hide();
		}
	  }
		
		/*check stream*/
		
		$("#fresher_graduation_stream").focusout(function(){
		  	
		  validate_fresher_graduation_stream();
		
		});
		
	  function validate_fresher_graduation_stream()
	  {
		  var i=$("#fresher_graduation_stream").val();
		  
	      if(i=="select stream")
	      {
			
			$("#_fresher_graduation_stream").html("Please enter graduation stream");
			$("#_fresher_graduation_stream").show();
			}
		 else{
			gstream=true;
			$("#_fresher_graduation_stream").hide();
		}
		  
	  }
	  
	  
	  
	  
	  $("#lt1year_graduation_score").focusout(function()
	  {
		  
		 validate_lt1year_graduation_score();
		});
		
		function validate_lt1year_graduation_score(){
		  var gscore=$("#lt1year_graduation_score").val();
	  if(gscore.length<1)
	    {
			
			$("#_lt1year_graduation_score").html("Please enter graduation score");
			$("#_lt1year_graduation_score").show();
			}
		else if(score<30 || score>100){
			$("#_lt1year_graduation_score").html("Please enter graduation percentage between 30 and 100");
			$("#_lt1year_graduation_score").show();
		}
			else{
				gscore=true;
				}
	     
		  
	  }
		
		/*check stream*/
		
		$("#lt1year_graduation_stream").focusout(function(){
		  	
		 validate_lt1year_graduation_stream();
		
		});
		
		
		function validate_lt1year_graduation_stream(){
		  var i=$("#lt1year_graduation_stream").val();
		  
	  if(i=="select stream")
	    {
			
			$("#_lt1year_graduation_stream").html("Please enter graduation stream");
			$("#_lt1year_graduation_stream").show();
			}
		else{
			gstream=true;
			$("#_lt1year_graduation_stream").hide();
		}
		  
	  } 
		
		
		
	  
	  
	  
	  $("#fresher_graduation_college").focusout(function()
	  {
		  validate_fresher_graduation_college();
		 
	  });
	  
	  function validate_fresher_graduation_college(){
		  var len=$("#fresher_graduation_college").val().length;
	  if(len<1)
	    {
			gcollege=false;
			$("#_fresher_graduation_college").html("Please enter graduation college/university");
			$("#_fresher_graduation_college").show();
			}
		else{
			gcollege=true;
			$("#_fresher_graduation_college").hide();
		}	
	     
		  
	  }
	  
	  
	  
	  $("#lt1year_graduation_college").focusout(function()
	  {
		  
		 	validate_lt1year_graduation_college();
	    
	  });
	  
	  function validate_lt1year_graduation_college(){
		  var len=$("#lt1year_graduation_college").val().length;
	  if(len<1)
	    {
			
			$("#_lt1year_graduation_college").html("Please enter graduation college/university");
			$("#_lt1year_graduation_college").show();
			}
		else{
			gcollege=true;
			$("#_lt1year_graduation_college").hide();
		} 
		  
	  }
	  
	  
	  $("#fresher_graduation_year").focusout(function()
	  {
		  
		 	validate_fresher_graduation_year();
	    
	  });
	  
	  function validate_fresher_graduation_year(){
		  var year=$("#fresher_graduation_year").val();
	  if(year.length<1)
	    {
			
			$("#_fresher_graduation_year").html("Please enter graduation passout year");
			$("#_fresher_graduation_year").show();
			}
			
		else if(year<1970 || year>2017){
			$("#_fresher_graduation_year").html("Please enter graduation passout year between 1970 and 2017");
			$("#_fresher_graduation_year").show();
			}	
		else{
			gyear=true;
			$("#_fresher_graduation_year").hide();
		} 
		  
	  }
	  
	 
		 $("#lt1year_graduation_year").focusout(function()
	  {
		  
		  	validate_lt1year_graduation_year();
	    
	  });
	 
	  function validate_lt1year_graduation_year(){
		  var year=$("#lt1year_graduation_year").val();
	  if(year.length<1)
	    {
			
			$("#_lt1year_graduation_year").html("Please enter graduation passout year");
			$("#_lt1year_graduation_year").show();
			}
			
			else if(year<1970 || year>2017){
			$("#_lt1year_graduation_year").html("Please enter graduation passout year between 1970 and 2017");
			$("#_lt1year_graduation_year").show();
			}
		else{
			gyear=true;
			$("#_lt1year_graduation_year").hide();
		}
		  
	  }
	  
	 
	  
	  
	  
	  
	  $("#fresher").click(function()
	  {
		  $(".lt1year_details").hide();
		  $(".gt1year_details").hide();
		  $(".fresher_details").show();
		  
	  });
	  
	  $("#lt1year").click(function()
	  {
		  $(".fresher_details").hide();
		  $(".gt1year_details").hide();
		  $(".lt1year_details").show();
		  
	  });
	  
	  $("#gt1year").click(function()
	  {
		  $(".fresher_details").hide();
		  $(".lt1year_details").hide();
		  $(".gt1year_details").show();
		  
	  });
	  
	  
	  
	  
	  
	  
	  
	  
	 
 
	  
  
      function validate_name()
	  {
	       var len=$("#name").val().length;
		     if(len<5||len>20)
			 {    
		        nam=false;
	           $("#_name").html("name should be between 5-20 characters long");
			   $("#_name").show();
             }
			 
			 else
			 {
				 nam=true;
			  $("#_name").hide();
			 }
			 
      }
  
  
  
  
  
     function validate_email()
	 {
	    
	 
	     var em=$("#email").val();
		 
		 var pattern= /^([A-Za-z0-9_\-\.]){1,}\@([A-Za-z0-9_\-\.]){1,}\.([A-Za-z]{2,4})$/;
	 
	    if (pattern.test(em)===false)
        {    
	        emai=false;
	        $("#_email").html("Invalid email");
			   $("#_email").show();
	
	
           
        }   
        else{
		      
	     emai=true;
		 $("#_email").hide();
        
	   } 
	
	  
	}
  
  
  
     
  
  
  
    function validate_phone()
	{
	   
	   var pat=/[^0-9]/;
	  
	    
		var i=$("#phone").val();
		var l=$("#phone").val().length;
		
	
	  if((pat.test(i))||l!==10)
	   {
	     phon=false;
        $("#_phone").html("Invalid phone");	   
	    $("#_phone").show(); 
	   }
      else
	  {
		  phon=true;
	   $("#_phone").hide();
	  }	
	
	
	}
 
$("#resume").focusout(function()
{
	
	validate_resume();
	
});

function validate_resume(){
	if($("#resume").val()==""){
		$("#_resume").html("Please upload your resume");
		$("#_resume").show();
		return false;
	}
	else{
		resume=true;
		$("#_resume").hide();
		return true;
	}	  
		  
	  }
	  
	  
	  $("input[type='radio'][name='Work Experience']").focusout(function()
       {
	
	       validate_work_experience();
	
      });
	  
	  
	function validate_work_experience(){
		
		var v=$("input[type='radio'][name='Work Experience']:checked").val();
		 // alert(v);
		  if(v==undefined)
		  {
			$("#_Work_Experience").html("Please select work experience");
		    $("#_Work_Experience").show();
  
			  
		  }
		  else
		  {
			$("#_Work_Experience").hide();  
		  }
	  }  


 
 $("#submit_btn").click(function()
	  {
		  
				validate_name();
				validate_email();
				 validate_phone();
				 validate_day();
				 validate_month();
				 validate_year();
				 validate_work_experience();
				validate_fresher10th();
				validate_fresher12th();
				validate_lt1year10th();
				validate_lt1year12th();
				validate_fresher_graduation_score();
				validate_fresher_graduation_stream();
				validate_lt1year_graduation_score();
				validate_lt1year_graduation_stream();
				validate_fresher_graduation_college();
				validate_lt1year_graduation_college();
				validate_fresher_graduation_year();
				validate_lt1year_graduation_year();
				validate_resume();
		  
		  
		  
		  
		  
		  
		  
		  
	 
		  if(nam==true && emai==true && phon==true && score_10th==true && score_12th==true && gcollege==true && gyear==true && gstream==true && resume==true)                                                          
		   {
				$("#custom_dialog_success_text").html("Hi"+" "+$("#name").val() + "<br />" + "Your details have been submitted successfully");
				
				$("#custom_dialog_error").hide();
				$("#custom_dialog_success").show();
		   }
		
		   else {
			  $("#custom_dialog_error_text").html("Hi"+" "+ $("#name").val() +" <br />" + "Please fill in all details");       
               
				$("#custom_dialog_success").hide();
				$("#custom_dialog_error").show();
	
              
	  
		     }
			   });
	  
	  
	$("#okbtn").click(function(){
		
		$("#custom_dialog_error").hide();
		
	});  
 
 
 
  
  
  
});  
  
  
  
  
  
  
  